package com.example.bean;

//import com.saperion.intf.SaFieldDescription;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by evajur on 2016.02.02.
 */
public class SaperionServerRMI {
    private String brokerPort;
    private String brokerHost;
    private String replyPort;
    private Map<String, String> properties = new HashMap<String, String>();
//    private SaFieldDescription[] saFieldDescriptions;

    public SaperionServerRMI(String brokerPort, String brokerHost, String replyPort) {
        this.brokerPort = brokerPort;
        this.brokerHost = brokerHost;
        this.replyPort = replyPort;
    }
    public String getReplyPort() {
        return replyPort;
    }

    public void setReplyPort(String replyPort) {
        this.replyPort = replyPort;
    }

    public String getBrokerPort() {
        return brokerPort;
    }

    public void setBrokerPort(String brokerPort) {
        this.brokerPort = brokerPort;
    }

    public String getBrokerHost() {
        return brokerHost;
    }

    public void setBrokerHost(String brokerHost) {
        this.brokerHost = brokerHost;
    }

//    public void setSaFieldDescriptions(SaFieldDescription[] saFieldDescriptions) {
//        this.saFieldDescriptions = saFieldDescriptions;
//    }
//
//    public Properties getNonSystemProperties(){
//        Properties properties = new Properties();
//
//        if(saFieldDescriptions != null){
//            for(SaFieldDescription fieldDescription : saFieldDescriptions){
//                if(fieldDescription.isNonSystemField()){
//                    properties.put(fieldDescription.getName(), fieldDescription.getName());
//                }
//            }
//        }
//        return properties;
//    }
}
