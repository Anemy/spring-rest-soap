package com.example.bean;

/**
 * Created by IntelliJ IDEA.
 * User: andgri
 * Date: 12.1.6
 * Time: 12.17
 * To change this template use File | Settings | File Templates.
 */
public class SaperionUser {
    private String userName;
    private Ddc ddc;

    private SaperionServerRMI serverRMI;

    public SaperionUser(String userName, Ddc ddc, SaperionServerRMI serverRMI) {
        this.userName = userName;
        this.ddc = ddc;
        this.serverRMI = serverRMI;
    }

    public String getUserName() {
        return userName;
    }

    public Ddc getDdc() {
        return ddc;
    }

    public SaperionServerRMI getServerRMI() {
        return serverRMI;
    }

    public void setServerRMI(SaperionServerRMI serverRMI) {
        this.serverRMI = serverRMI;
    }

    @Override
    public String toString() {
        return "SaperionUser{" +
                "userName='" + userName + '\'' +
                ", ddc=" + ddc +
                ", serverRMI=" + serverRMI +
                '}';
    }
}
