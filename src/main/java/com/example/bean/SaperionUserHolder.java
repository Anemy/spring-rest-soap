package com.example.bean;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: andgri
 * Date: 12.1.11
 * Time: 13.11
 * To change this template use File | Settings | File Templates.
 */
public class SaperionUserHolder {
    private Map<String, SaperionUser> saperionUsers;

    public Map<String, SaperionUser> getSaperionUsers() {
        return saperionUsers;
    }

    public void setSaperionUsers(Map<String, SaperionUser> saperionUsers) {
        this.saperionUsers = saperionUsers;
    }
}
