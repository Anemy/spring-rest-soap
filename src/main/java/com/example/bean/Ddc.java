package com.example.bean;

//import com.saperion.intf.SaFieldDescription;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: andgri
 * Date: 12.1.6
 * Time: 12.26
 * To change this template use File | Settings | File Templates.
 */
public class Ddc {
    private String name;
    private String searhId;
    private String pageNumberField;
    private Map<String, String> properties = new HashMap<String, String>();
//    private SaFieldDescription[] saFieldDescriptions;

    public Ddc(String name, String searhId) {
        this.name = name;
        this.searhId = searhId;
    }
    public Ddc(String name, String searhId, String pageNumberField) {
        this.name = name;
        this.searhId = searhId;
        this.pageNumberField = pageNumberField;
    }


    public String getPageNumberField() {
        return pageNumberField;
    }

    public void setPageNumberField(String pageNumberField) {
        this.pageNumberField = pageNumberField;
    }


    public String getName() {
        return name;
    }

    public String getSearhId() {
        return searhId;
    }

//    public void setSaFieldDescriptions(SaFieldDescription[] saFieldDescriptions) {
//        this.saFieldDescriptions = saFieldDescriptions;
//    }
//
//    public Properties getNonSystemProperties(){
//        Properties properties = new Properties();
//
//        if(saFieldDescriptions != null){
//            for(SaFieldDescription fieldDescription : saFieldDescriptions){
//                if(fieldDescription.isNonSystemField()){
//                    properties.put(fieldDescription.getName(), fieldDescription.getName());
//                }
//            }
//        }
//
//        return properties;
//    }
}
