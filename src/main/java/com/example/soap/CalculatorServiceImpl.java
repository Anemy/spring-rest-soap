package com.example.soap;

import com.example.bean.RandomText;
import com.example.bean.SaperionUserHolder;
import com.example.service.CalculationService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jws.WebService;

@WebService(endpointInterface = "com.example.soap.CalculatorService")
public class CalculatorServiceImpl implements CalculatorService {

    @Autowired
    private CalculationService calculationService;
    @Autowired
    private RandomText randomText;
    @Autowired
    private SaperionUserHolder userHolder;

    public String randomNumber() {
        return "randomText: " + randomText.getText() + "| calculationService:" + calculationService.something()
                + "| userHolder: " + userHolder.getSaperionUsers().get("test");
    }
}
