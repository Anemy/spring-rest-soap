package com.example.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/songs")
//@Consumes({"application/json"})
//@Produces({"application/json"})
public class SongService {

    private SongRepository songRepository = new SongRepository();

    @GET
    @Path("/{id}")
//    @Consumes({"application/json"})
//    @Produces({"application/json"})
    public String getSongById(@PathParam("id") String id) {
        return songRepository.findById(id);
    }
}
